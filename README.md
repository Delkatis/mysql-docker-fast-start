# MySql fast start!
Fast setup of mysql without installations

## REQUIRE:
 - docker
 - docker-compose

### Go in root directory and run:

```
docker-compose up
```
### OR in detached mode:

```
docker-compose up -d
```

### Now you can see on wich host the process is listening with:

```
docker ps
```

## connect with:
- user: root
- pass: root